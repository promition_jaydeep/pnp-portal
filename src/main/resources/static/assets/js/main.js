/**
 * Common Java script functions for application
 */

// Make all Charts responsive
Chart.defaults.global.responsive = true;

$(function() {
	/**
	 * Converts input field (having css class ".upper-case") value to upper case
	 */
    $('input.upper-case').keyup(function() {
        this.value = this.value.toUpperCase();
    });
    
    /**
	 * Converts input field (having css class ".lower-case") value to lower case
	 */
    $('input.lower-case').keyup(function() {
        this.value = this.value.toLowerCase();
    });

    /**
	 * Trims leading and trailing spaces from all input field values
	 */
    $('input').focusout(function() {
    	 this.value =$.trim(this.value);
    });
    

    var url = window.location;
    // Will only work if string in href matches with location
    $('.sidebar-menu li a[href="' + url + '"]').parent().addClass('active');
    // Will also work for relative and absolute hrefs
    $('.sidebar-menu li a').filter(function() {
        return this.href == url;
    }).parent().parent().parent().addClass('active');
    
  //Enable sidebar dinamic menu
   // $.AdminLTE.dinamicMenu();
    /* DinamicMenu()
     * dinamic activate menu
     */
    /*$.AdminLTE.dinamicMenu = function() {
        var url = window.location;
        // Will only work if string in href matches with location
        $('.treeview-menu li a[href="' + url + '"]').parent().addClass('active');
        // Will also work for relative and absolute hrefs
        $('.treeview-menu li a').filter(function() {
            return this.href == url;
        }).parent().parent().parent().addClass('active');
    };*/
        
});


/**
 * Replaces \n with <br /> to view in HTML
 */
function toHTMLText(str) {
	if(str) {
		return str.replace(/\n/g, "<br />")
	}
}