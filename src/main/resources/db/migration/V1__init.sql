/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_roles_name` (`name`)
) DEFAULT CHARSET=utf8;


/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `account_non_expired` bit(1) DEFAULT NULL,
  `account_non_locked` bit(1) DEFAULT NULL,
  `credentials_non_expired` bit(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_users_username` (`username`)
) DEFAULT CHARSET=utf8;

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK_user_roles_role_id` (`role_id`),
  CONSTRAINT `FK_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) DEFAULT CHARSET=utf8;

/*Table structure for table `complaint_types` */

DROP TABLE IF EXISTS `complaint_types`;

CREATE TABLE `complaint_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resolution_days` int(11) DEFAULT NULL,
  `assign_to` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_complaint_types_assign_to` (`assign_to`),
  CONSTRAINT `FK_complaint_types_assign_to` FOREIGN KEY (`assign_to`) REFERENCES `users` (`id`)
) DEFAULT CHARSET=utf8;


/*Table structure for table `complaint_assignment` */

DROP TABLE IF EXISTS `complaint_assignment`;

CREATE TABLE `complaint_assignment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `ward_no` varchar(255) DEFAULT NULL,
  `assign_to` bigint(20) NOT NULL,
  `complaint_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_complaint_assignment_assign_to` (`assign_to`),
  KEY `FK_complaint_assignment_complaint_type_id` (`complaint_type_id`)
) DEFAULT CHARSET=utf8;

/*Table structure for table `complaints` */

DROP TABLE IF EXISTS `complaints`;

CREATE TABLE `complaints` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `file` tinyblob,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `ward_no` varchar(255) NOT NULL,
  `complaint_type_id` bigint(20) NOT NULL,
  `complaint_no` varchar(255) NOT NULL,
  `resolution_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_complaints_complaint_type_id` (`complaint_type_id`),
  CONSTRAINT `FK_complaints_complaint_type_id` FOREIGN KEY (`complaint_type_id`) REFERENCES `complaint_types` (`id`)
) DEFAULT CHARSET=utf8;

/*Table structure for table `complaint_comments` */

DROP TABLE IF EXISTS `complaint_comments`;

CREATE TABLE `complaint_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `complaint_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_complaint_comments_complaint_id` (`complaint_id`),
  CONSTRAINT `FK_complaint_comments_complaint_id` FOREIGN KEY (`complaint_id`) REFERENCES `complaints` (`id`)
) DEFAULT CHARSET=utf8;



