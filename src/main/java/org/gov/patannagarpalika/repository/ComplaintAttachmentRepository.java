package org.gov.patannagarpalika.repository;

import org.gov.patannagarpalika.model.ComplaintAttachment;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintAttachmentRepository extends DataTablesRepository<ComplaintAttachment, Long>{

}
