package org.gov.patannagarpalika.repository;

import java.util.List;

import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.model.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintTypeRepository extends DataTablesRepository<ComplaintType, Long>{
	
	ComplaintType findByName(String name);
	ComplaintType findByNameIgnoreCase(String name);
	List<ComplaintType> findAllByAssignTo(User user);
	
}
