package org.gov.patannagarpalika.repository;

import java.util.List;

import org.gov.patannagarpalika.model.User;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends DataTablesRepository<User, Long> {
	User findByUsername(String username);
	User findByUsernameIgnoreCase(String username);
	List<User> findAllByEnabled(boolean enabled);
}
