package org.gov.patannagarpalika.repository;


import org.gov.patannagarpalika.model.Role;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends DataTablesRepository<Role, Long> {

	Role findByName(String roleName); 
}
