package org.gov.patannagarpalika.repository;

import java.util.List;

import org.gov.patannagarpalika.constant.ComplaintStatusEnum;
import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.model.ComplaintType;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintRepository extends DataTablesRepository<Complaint, Long> {

	boolean existsByComplaintNo(String complaintNo);
	List<Complaint> findAllByComplaintType(ComplaintType complaintType);
	List<Complaint> findAllByStatus(ComplaintStatusEnum status);
	List<Complaint> findAllByComplaintTypeAndStatus(ComplaintType complaintType, ComplaintStatusEnum status);
	
	long countByStatus(ComplaintStatusEnum status);
	long countByComplaintType(ComplaintType complaintType);
	
}
