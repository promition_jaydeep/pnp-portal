/**
 * 
 */
package org.gov.patannagarpalika.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.gov.patannagarpalika.constant.ComplaintStatusEnum;
import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jjvirani
 *
 */
@Transactional
@Service
public class ComplaintServiceImpl implements ComplaintService {

	@Autowired
	private ComplaintRepository complaintRepository;
	
	/* (non-Javadoc)
	 * @see org.gov.patannagarpalika.service.ComplaintService#save(org.gov.patannagarpalika.model.Complaint)
	 */
	@Override
	public Complaint save(Complaint complaint) {
		
		String complaintNo = null;
		
		// Generate unique complaint number for new complaint
		do {
			complaintNo =  RandomStringUtils.randomAlphanumeric(6).toUpperCase();
		} while (complaintRepository.existsByComplaintNo(complaintNo));
		
		complaint.setComplaintNo(complaintNo);
		
		// Set initial status
		if(complaint.getStatus() == null) {
			complaint.setStatus(ComplaintStatusEnum.OPEN);
		} else if(ComplaintStatusEnum.CLOSED.equals(complaint.getStatus())) {
			// If status is updated to closed, then update resolution date
			complaint.setResolutionDate(new Date());
		} else {
			// If status is not closed, then remove resolution date
			complaint.setResolutionDate(null);
		}
		
		complaintRepository.save(complaint);
		
		return complaint;
	}
	

}
