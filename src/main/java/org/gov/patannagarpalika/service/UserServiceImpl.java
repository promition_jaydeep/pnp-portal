/**
 * 
 */
package org.gov.patannagarpalika.service;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author jjvirani
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
    private UserRepository userRepository;
	
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    

	@Override
	public void save(User user) {
		logger.info("userServiceImpl : save() : START");
		
		if(!isHashed(user.getPassword())) {
			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		}
		
        userRepository.save(user);
        logger.info("userServiceImpl : save() : END");
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	private boolean isHashed(String string) {
		boolean isHashed = false;
		
		// NEVER log input string, as it can be password
		
		if(StringUtils.isNotBlank(string) 
				&& string.startsWith("$") 
				&& string.length() == 60) {
			isHashed = true;
		}
		
		return isHashed;
	}
}
