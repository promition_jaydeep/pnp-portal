/**
 * 
 */
package org.gov.patannagarpalika.service;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.constant.RoleEnum;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.model.Role;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.gov.patannagarpalika.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author jjvirani
 *
 */
@Service
@Transactional
public class MetaDataServiceImpl implements MetaDataService {

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserService userService;
	
	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@Autowired
	private ComplaintService complaintService;
	
	@Autowired
	private ComplaintRepository complaintRepostory;

	@Override
	public void createMetadata() {
		addRoles();
		addUsers();
		addComplaintTypes();
	}

	@Override
	public void addRoles() {

		Role adminRole = roleRepository.findByName(RoleEnum.ADMIN.name());
		if (adminRole == null) {
			roleRepository.save(new Role(RoleEnum.ADMIN.name()));
		}

		Role userRole = roleRepository.findByName(RoleEnum.USER.name());
		if (userRole == null) {
			roleRepository.save(new Role(RoleEnum.USER.name()));
		}

	}

	@Override
	public void addUsers() {
		User adminUser = userService.findByUsername(RoleEnum.ADMIN.name());

		if (adminUser == null) {
			adminUser = new User();
			adminUser.setUsername(RoleEnum.ADMIN.name().toLowerCase());
			adminUser.setPassword(RoleEnum.ADMIN.name().toLowerCase());
			adminUser.setFirstName("Patan");
			adminUser.setLastName("Nagarpalika");
			adminUser.setEnabled(true);

			Role adminRole = roleRepository.findByName(RoleEnum.ADMIN.name());
			adminUser.addRole(adminRole);

			userService.save(adminUser);
		}
	}

	@Override
	public void addComplaintTypes() {
		

			User adminUser = userService.findByUsername(RoleEnum.ADMIN.name());
			ComplaintType complaintType = complaintTypeRepository.findByName(AppConstants.COMPLAINT_TYPE_OTHER);
			
			if(complaintType == null) {
				complaintType = new ComplaintType();
				complaintType.setName(AppConstants.COMPLAINT_TYPE_OTHER);
				complaintType.setResolutionDays(AppConstants.COMPLAINT_RESOLUTION_DAYS);
				complaintType.setAssignTo(adminUser);
				complaintTypeRepository.save(complaintType);
			}

		
	}

}
