/**
 * 
 */
package org.gov.patannagarpalika.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jjvirani
 *
 */
@Service
@Transactional
public class ComplaintTypeServiceImpl implements ComplaintTypeService{

	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@Autowired
	private ComplaintRepository complaintRepository;
	
	@Override
	public void delete(ComplaintType complaintType) {
		ComplaintType otherComplaintType = complaintTypeRepository.findByNameIgnoreCase(AppConstants.COMPLAINT_TYPE_OTHER);
		
		// Transfer complaints of old type to other
		List<Complaint> complaints = complaintRepository.findAllByComplaintType(complaintType);
		
		if(CollectionUtils.isNotEmpty(complaints)) {
			complaints.forEach(complaint -> complaint.setComplaintType(otherComplaintType));
			complaintRepository.saveAll(complaints);
		}
		
		complaintTypeRepository.delete(complaintType);
		
	}

}
