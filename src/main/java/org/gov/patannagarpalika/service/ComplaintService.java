/**
 * 
 */
package org.gov.patannagarpalika.service;

import org.gov.patannagarpalika.model.Complaint;

/**
 * @author jjvirani
 *
 */
public interface ComplaintService {
	Complaint save(Complaint complaint);
}
