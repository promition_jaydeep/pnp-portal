/**
 * 
 */
package org.gov.patannagarpalika.service;

import javax.transaction.Transactional;

import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author jjvirani
 *
 */
@Service("customUserDetailsService")
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private static Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("loadUserByUsername() : START ");
		User user = userRepository.findByUsernameIgnoreCase(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		logger.info("loadUserByUsername() : END ");
		return user;
	}

}
