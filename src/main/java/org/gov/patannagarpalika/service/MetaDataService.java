/**
 * 
 */
package org.gov.patannagarpalika.service;

/**
 * @author jjvirani
 *
 */
public interface MetaDataService {
	
	void createMetadata();
	
	void addRoles();
	
	void addUsers();
	
	void addComplaintTypes();

}
