/**
 * 
 */
package org.gov.patannagarpalika.service;

import org.gov.patannagarpalika.model.User;

/**
 * @author jjvirani
 *
 */
public interface UserService {
	
	void save(User user);
	
	User findByUsername(String username);
	
}
