/**
 * 
 */
package org.gov.patannagarpalika.dto;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.collect.Lists;

/**
 * @author jjvirani
 *
 */
public class MessageDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_EMPTY)
	private List<String> successMessages;
	
	@JsonInclude(Include.NON_EMPTY)
	private List<String> errorMessages;
	
	@JsonInclude(Include.NON_EMPTY)
	private List<String> infoMessages;
	
	@JsonInclude(Include.NON_EMPTY)
	private List<String> warningMessages;
	
	@JsonInclude(Include.NON_EMPTY)
	private Map<String, String> fieldErrors;
	
	public List<String> getSuccessMessages() {
		return successMessages;
	}
	public List<String> getErrorMessages() {
		return errorMessages;
	}
	public List<String> getInfoMessages() {
		return infoMessages;
	}
	public List<String> getWarningMessages() {
		return warningMessages;
	}
	public Map<String, String> getFieldErrors() {
		return fieldErrors;
	}
	
	public void addSuccessMessages(String ... successMessages) {
		if(this.successMessages == null) {
			this.successMessages = new LinkedList<>();
		}
		this.successMessages.addAll(Lists.newArrayList(successMessages));
	}
	
	public void addErrorMessages(String ... errorMessages) {
		if(this.errorMessages == null) {
			this.errorMessages = new LinkedList<>();
		}
		this.errorMessages.addAll(Lists.newArrayList(errorMessages));
	}
	
	public void addInfoMessages(String ... infoMessages) {
		if(this.infoMessages == null) {
			this.infoMessages = new LinkedList<>();
		}
		this.infoMessages.addAll(Lists.newArrayList(infoMessages));
	}
	
	public void addWarningMessages(String ... warningMessages) {
		if(this.warningMessages == null) {
			this.warningMessages = new LinkedList<>();
		}
		this.warningMessages.addAll(Lists.newArrayList(warningMessages));
	}
	
	
	public void addFieldError(String field, String errorMessage) {
		if(this.fieldErrors == null) {
			this.fieldErrors = new LinkedHashMap<>();
		}
		this.fieldErrors.put(field, errorMessage);
	}
	
	public boolean isEmpty() {
		return CollectionUtils.isEmpty(successMessages)
				&& CollectionUtils.isEmpty(errorMessages)
				&& CollectionUtils.isEmpty(infoMessages)
				&& CollectionUtils.isEmpty(warningMessages)
				&& MapUtils.isEmpty(fieldErrors);
	}
	
	public boolean hasSuccesses() {
		return CollectionUtils.isNotEmpty(successMessages);
	}

	public boolean hasErrors() {
		return CollectionUtils.isNotEmpty(errorMessages);
	}
	
	public boolean hasInfos() {
		return CollectionUtils.isNotEmpty(infoMessages);
	}
	
	public boolean hasWarnings() {
		return CollectionUtils.isNotEmpty(warningMessages);
	}
	
	public boolean hasFieldErrors() {
		return MapUtils.isNotEmpty(fieldErrors);
	}
	
	public String getFieldError(String fieldName) {
		
		String fieldError = null;
		
		if(MapUtils.isNotEmpty(fieldErrors)) {
			fieldError = fieldErrors.get(fieldName);
		}
		
		return fieldError;
	}
	
	public boolean hasFieldError(String fieldName) {
		return StringUtils.isNotBlank(getFieldError(fieldName));
	}
}
