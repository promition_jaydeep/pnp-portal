package org.gov.patannagarpalika.dto;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RestResponseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_EMPTY)
	private String message;
	
	private boolean status = true;
	
	private Object data;
	
	@JsonInclude(Include.NON_EMPTY)
	private Map<String, String> fieldErrors;
	
	public RestResponseDTO() {}
	
	public RestResponseDTO(boolean status) {
		this.status = status;
	}
	
	public RestResponseDTO(Object data) {
		this.data = data;
	}
	
	public RestResponseDTO(Object data, String message) {
		this(data);
		this.message = message;
	}
	
	public RestResponseDTO(boolean status, String message) {
		this(status);
		this.message = message;
	}
	
	public void addFieldError(String field, String errorMessage) {
		if(this.fieldErrors == null) {
			this.fieldErrors = new LinkedHashMap<>();
		}
		this.fieldErrors.put(field, errorMessage);
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Map<String, String> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(Map<String, String> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

}
