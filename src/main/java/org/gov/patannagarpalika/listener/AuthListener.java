/**
 * 
 */
package org.gov.patannagarpalika.listener;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

/**
 * @author jjvirani
 *
 */
@Component
@Transactional
public class AuthListener {


	@Autowired
	private UserService userService;
	
	
	@Autowired
	private HttpSession session;

	@EventListener
	public void handleAuthSuccessEvent(InteractiveAuthenticationSuccessEvent event) {
		String userName =  event.getAuthentication().getName();
		User user = userService.findByUsername(userName);
				
		session.setAttribute(AppConstants.CURRENT_USER, user);
		
	}
	
}