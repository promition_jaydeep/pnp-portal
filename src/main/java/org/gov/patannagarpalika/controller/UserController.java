/**
 * 
 */
package org.gov.patannagarpalika.controller;

import java.util.Map;

import org.gov.patannagarpalika.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jjvirani
 *
 */
@Controller
@RequestMapping("/users")
public class UserController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private RoleRepository roleRepository;
	
	@RequestMapping(path= {""})
	public String list(Map<String, Object> model) {
		logger.info("list() : START");
		
		model.put("roles", roleRepository.findAll());
		
		logger.info("list() : END");
		return "user/list";
	}
	
}
