/**
 * 
 */
package org.gov.patannagarpalika.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.constant.ComplaintStatusEnum;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jjvirani
 *
 */
@Controller
public class DashboardController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(DashboardController.class);
	
	@Autowired
	private ComplaintRepository complaintRepository;
	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@RequestMapping(path= {"/", "/dashboard"})
	public String dashboard(Map<String, Object> model, HttpSession session) {
		logger.info("dashboard() : START");
		
		User currentUser = (User)session.getAttribute(AppConstants.CURRENT_USER);
		
		long total = complaintRepository.count();
		
		Map<String, Long> byStatusCountMap = new LinkedHashMap<>();
		
		for(ComplaintStatusEnum complaintStatus : ComplaintStatusEnum.values()) {
			byStatusCountMap.put(complaintStatus.getDisplayName(), complaintRepository.countByStatus(complaintStatus));
		}
		
		Map<String, Long> byTypeCountMap = new LinkedHashMap<>();
		
		for(ComplaintType complaintType : complaintTypeRepository.findAll()) {
			byTypeCountMap.put(complaintType.getName(), complaintRepository.countByComplaintType(complaintType));
		}
		
		model.put("total", total);
		model.put("byStatusCountMap", byStatusCountMap);
		model.put("byTypeCountMap", byTypeCountMap);
		
		logger.info("dashboard() : END");
		return "dashboard";
		
	}
	
}
