/**
 * 
 */
package org.gov.patannagarpalika.controller;

import java.util.Map;

import org.gov.patannagarpalika.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jjvirani
 *
 */
@Controller
@RequestMapping("/complaintTypes")
public class ComplaintTypeController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(ComplaintTypeController.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(path= {""})
	public String list(Map<String, Object> model) {
		logger.info("list() : START");
		
		model.put("users", userRepository.findAllByEnabled(true));
		
		logger.info("list() : END");
		return "complaintType/list";
	}
	
}
