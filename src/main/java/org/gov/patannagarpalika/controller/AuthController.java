/**
 * 
 */
package org.gov.patannagarpalika.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author jjvirani
 *
 */
@Controller
public class AuthController extends BaseController {
	 
	private static  Logger logger = LoggerFactory.getLogger(AuthController.class);
	
	@GetMapping("/login")
	public String login() {
		logger.info("login() : START");
		return "auth/login";
	}

	
	@GetMapping("/access-denied")
    public String accessDenied() {
		logger.error("accessDenied()");
        return "auth/access-denied";
    }
}
