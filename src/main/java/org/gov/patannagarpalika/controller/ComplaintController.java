/**
 * 
 */
package org.gov.patannagarpalika.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jjvirani
 *
 */
@Controller
@RequestMapping("/complaints")
public class ComplaintController extends BaseController {
	
	private static Logger logger = LoggerFactory.getLogger(ComplaintController.class);
	
	@RequestMapping(path= {""})
	public String list(Map<String, Object> model) {
		logger.info("list() : START");
		
		logger.info("list() : END");
		return "complaint/list";
	}
	
}
