/**
 * 
 */
package org.gov.patannagarpalika.controller;

import javax.servlet.http.HttpSession;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author jjvirani
 *
 */
public abstract class BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	@Autowired
	protected HttpSession session;

	
	public User getCurrentUser() {
		User currentUser = null;
		if(session != null) {
			currentUser = (User)session.getAttribute(AppConstants.CURRENT_USER);
		}
		
		return currentUser;
	}
	
}
