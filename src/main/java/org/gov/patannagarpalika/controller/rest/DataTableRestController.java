/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.constant.RoleEnum;
import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.gov.patannagarpalika.repository.UserRepository;
import org.gov.patannagarpalika.specification.ComplaintAssignToSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/data")
public class DataTableRestController extends BaseRestController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@Autowired
	private ComplaintRepository complaintRepository;

	@JsonView(DataTablesOutput.View.class)
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public DataTablesOutput<User> getUsers(@Valid @RequestBody DataTablesInput input) {
		DataTablesOutput<User> result = userRepository.findAll(input);
		return result;
	}
	
	@JsonView(DataTablesOutput.View.class)
	@RequestMapping(value = "/complaintTypes", method = RequestMethod.POST)
	public DataTablesOutput<ComplaintType> getComplaintTypes(@Valid @RequestBody DataTablesInput input) {
		DataTablesOutput<ComplaintType> result = complaintTypeRepository.findAll(input);
		return result;
	}
	
	@JsonView(DataTablesOutput.View.class)
	@RequestMapping(value = "/complaints", method = RequestMethod.POST)
	public DataTablesOutput<Complaint> getComplaints(@Valid @RequestBody DataTablesInput input, HttpSession session) {
		
		
		User currentUser = (User)session.getAttribute(AppConstants.CURRENT_USER);
		
		DataTablesOutput<Complaint> result = null;
		if(currentUser.hasRole(RoleEnum.ADMIN.name())) {
			// If not admin user,  show all complaints
			result = complaintRepository.findAll(input);
		} else {
			// If not admin user, only show complaint assigned to user only
			List<ComplaintType> userComplaintTypes = complaintTypeRepository.findAllByAssignTo(currentUser);
			Specification<Complaint> assignToSepc = new ComplaintAssignToSpecification(userComplaintTypes);
			result = complaintRepository.findAll(input, assignToSepc);
			
			
		}
		
		return result;
	}
}
