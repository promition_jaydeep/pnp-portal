/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import org.gov.patannagarpalika.dto.RestResponseDTO;
import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.model.ComplaintAttachment;
import org.gov.patannagarpalika.model.ComplaintComment;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.repository.ComplaintAttachmentRepository;
import org.gov.patannagarpalika.repository.ComplaintRepository;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.gov.patannagarpalika.service.ComplaintService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/api/complaints")
public class ComplaintRestController extends BaseRestController {

	private static Logger logger = LoggerFactory.getLogger(ComplaintRestController.class);
	
	@Autowired
	private ComplaintRepository complaintRepository;

	@Autowired
	private ComplaintService complaintService;

	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;

	@Autowired
	private ComplaintAttachmentRepository complaintAttachmentRepository;

	@GetMapping
	public RestResponseDTO readAll() {
		logger.info("readAll() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			restResponse.setData(complaintRepository.findAll());
		} catch (Exception e) {
			logger.error("Error occurred while getting complaint", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occurred while getting complaint.");
		}
		logger.info("readAll() : END");

		return restResponse;
	}

	@GetMapping("/{id}")
	public RestResponseDTO read(@PathVariable long id) {
		logger.info("read() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			Optional<Complaint> existingComplaintOptional = complaintRepository.findById(id);

			if (existingComplaintOptional.isPresent()) {
				restResponse.setData(existingComplaintOptional.get());
			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("Complaint does not exist.");
			}

		} catch (Exception e) {
			logger.error("Error occurred while getting complaint.", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occurred while getting complaint.");
		}

		logger.info("list() : END");
		return restResponse;
	}

	@PostMapping (produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public RestResponseDTO create(Complaint complaint,
                                  @RequestParam(required = false) MultipartFile attachmentFile) {
		logger.info("create() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			if(attachmentFile != null) {
                ComplaintAttachment attachment = new ComplaintAttachment();
                attachment.setComplaint(complaint);
                attachment.setFileName(StringUtils.cleanPath(attachmentFile.getOriginalFilename()));
                attachment.setFileType(attachmentFile.getContentType());
                attachment.setData(attachmentFile.getBytes());

                complaint.setAttachment(attachment);
            }
			complaintService.save(complaint);
			restResponse.setData(complaint);
		} catch (Exception e) {
			logger.error("Error occurred while creating complaint", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occurred while creating complaint");
		}
		logger.info("create() : END");
		return restResponse;
	}

	@PutMapping("/{id}")
	public RestResponseDTO update(Complaint complaint, @PathVariable long id) {
		logger.info("update() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {

			Optional<Complaint> existingComplaintOptional = complaintRepository.findById(id);

			if (existingComplaintOptional.isPresent()) {
				Complaint existingComplaint = existingComplaintOptional.get();
				
				existingComplaint.setStatus(complaint.getStatus());
				
				complaintService.save(existingComplaint);

				restResponse.setData(existingComplaint);

			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("Complaint dose not exist.");
			}
		} catch (Exception e) {
			logger.error("Error occurred while updating complaint", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occurred while updating complaint");
		}
		logger.info("update() : END");
		return restResponse;
	}
	
	@PostMapping("/{complaint}/comments")
	public RestResponseDTO addComment(@PathVariable Complaint complaint, ComplaintComment complaintComment) {
		logger.info("addComment() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			
			complaint.addComment(complaintComment);
			complaintService.save(complaint);
			restResponse.setData(complaintComment);
		} catch (Exception e) {
			logger.error("Error occurred while adding complaint comment", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occurred while creating complaint comment");
		}
		logger.info("addComment() : END");
		return restResponse;
	}

    @GetMapping("/attachments/{attachmentId}")
    public ResponseEntity<Resource> downloadAttachment(@PathVariable long attachmentId) {
        logger.info("downloadAttachment() : START");

        ResponseEntity response = null;

        Optional<ComplaintAttachment> attachmentOptional = complaintAttachmentRepository.findById(attachmentId);

        if(attachmentOptional.isPresent()) {
            ComplaintAttachment attachment = attachmentOptional.get();
            response =  ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(attachment.getFileType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + attachment.getFileName() + "\"")
                    .body(new ByteArrayResource(attachment.getData()));
        } else {
            response =  ResponseEntity.notFound().build();
        }


        logger.info("downloadAttachment() : END");
        return response;
    }
}
