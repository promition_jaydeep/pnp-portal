/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import javax.servlet.http.HttpServletResponse;

import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.gov.patannagarpalika.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/api/validation")
public class ValidationRestController extends BaseRestController {
	
	private static Logger logger = LoggerFactory.getLogger(ValidationRestController.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@GetMapping("/username")
	public String validateUsername(HttpServletResponse response, 
			@RequestParam(required=true) String username) {
		logger.info("validateUsername() : START");
		String errorMessage = "";
		User user = userRepository.findByUsernameIgnoreCase(username);
		if(user != null) {
			// Username already exists
			response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			errorMessage = "Username already exists.";
		} else {
			response.setStatus(HttpStatus.OK.value());
		}
		logger.info("validateUsername() : END");
		return errorMessage;
	}
	
	@GetMapping("/complaintType")
	public String validateComplaintType(HttpServletResponse response, 
			@RequestParam(required=true) String name) {
		logger.info("validateComplaintType() : START");
		String errorMessage = "";
		ComplaintType complaintType = complaintTypeRepository.findByNameIgnoreCase(name.trim());
		if(complaintType != null) {
			// Complaint Type already exists
			response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
			errorMessage = "Complaint Type already exists.";
		} else {
			response.setStatus(HttpStatus.OK.value());
		}
		logger.info("validateComplaintType() : END");
		return errorMessage;
	}
	

	
}
