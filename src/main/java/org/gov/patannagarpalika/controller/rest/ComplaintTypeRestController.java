/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import java.util.Optional;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.dto.RestResponseDTO;
import org.gov.patannagarpalika.model.ComplaintType;
import org.gov.patannagarpalika.repository.ComplaintTypeRepository;
import org.gov.patannagarpalika.service.ComplaintTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/api/complaintTypes")
public class ComplaintTypeRestController extends BaseRestController {

	private static Logger logger = LoggerFactory.getLogger(ComplaintTypeRestController.class);

	@Autowired
	private ComplaintTypeRepository complaintTypeRepository;
	
	@Autowired
	private ComplaintTypeService complaintTypeService;
	
	@GetMapping
	public RestResponseDTO readAll() {
		logger.info("readAll() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			restResponse.setData(complaintTypeRepository.findAll());
		} catch (Exception e) {
			logger.error("Error occured while getting complaint types", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while getting complaint types.");
		}
		logger.info("readAll() : END");

		return restResponse;
	}

	@GetMapping("/{id}")
	public RestResponseDTO read(@PathVariable long id) {
		logger.info("read() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			Optional<ComplaintType> existingComplaintTypeOptional = complaintTypeRepository.findById(id);

			if (existingComplaintTypeOptional.isPresent()) {
				restResponse.setData(existingComplaintTypeOptional.get());
			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("Complaint Type does not exist.");
			}

		} catch (Exception e) {
			logger.error("Error occured while getting complaint type.", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while getting complaint type.");
		}

		logger.info("list() : END");
		return restResponse;
	}

	@PostMapping
	public RestResponseDTO create(ComplaintType complaintType) {
		logger.info("create() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			complaintTypeRepository.save(complaintType);
			restResponse.setData(complaintType);
		} catch (Exception e) {
			logger.error("Error occured while creating complaint type", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while creating complaint type");
		}
		logger.info("create() : END");
		return restResponse;
	}

	@PutMapping("/{id}")
	public RestResponseDTO update(ComplaintType complaintType, @PathVariable long id) {
		logger.info("update() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {

			Optional<ComplaintType> existingComplaintTypeOptional = complaintTypeRepository.findById(id);

			if (existingComplaintTypeOptional.isPresent()) {
				ComplaintType existingComplaintType = existingComplaintTypeOptional.get();
				existingComplaintType.setName(complaintType.getName());
				existingComplaintType.setResolutionDays(complaintType.getResolutionDays());
				existingComplaintType.setAssignTo(complaintType.getAssignTo());

				complaintTypeRepository.save(existingComplaintType);

				restResponse.setData(existingComplaintType);

			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("Complaint Type dose not exist.");
			}
		} catch (Exception e) {
			logger.error("Error occured while updating complaint type", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while updating complaint type");
		}
		logger.info("update() : END");
		return restResponse;
	}
	
	@DeleteMapping("/{id}")
	public RestResponseDTO delete(@PathVariable long id) {
		logger.info("delete() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {

			Optional<ComplaintType> existingComplaintTypeOptional = complaintTypeRepository.findById(id);

			if (existingComplaintTypeOptional.isPresent()) {
				ComplaintType existingComplaintType = existingComplaintTypeOptional.get();
				
				if(AppConstants.COMPLAINT_TYPE_OTHER.equalsIgnoreCase(existingComplaintType.getName())) {
					restResponse.setStatus(false);
					restResponse.setMessage("Complaint Type 'Other' cannot be deleted.");
				} else {
					complaintTypeService.delete(existingComplaintType);
				}

			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("Complaint Type dose not exist.");
			}
		} catch (Exception e) {
			logger.error("Error occured while deleting complaint type", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while deleting complaint type");
		}
		
		logger.info("delete() : END");
		return restResponse;
	}
}
