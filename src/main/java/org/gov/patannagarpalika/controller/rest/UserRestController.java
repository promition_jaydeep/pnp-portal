/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import java.util.Optional;

import org.gov.patannagarpalika.dto.RestResponseDTO;
import org.gov.patannagarpalika.model.User;
import org.gov.patannagarpalika.repository.UserRepository;
import org.gov.patannagarpalika.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/api/users")
public class UserRestController extends BaseRestController {

	private static Logger logger = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@GetMapping
	public RestResponseDTO readAll() {
		logger.info("readAll() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			restResponse.setData(userRepository.findAll());
		} catch (Exception e) {
			logger.error("Error occured while getting users", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while getting users.");
		}
		logger.info("readAll() : END");

		return restResponse;
	}
	
	@GetMapping("/{id}")
	public RestResponseDTO read(@PathVariable long id) {
		logger.info("read() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			Optional<User> existingUserOptional = userRepository.findById(id);

			if (existingUserOptional.isPresent()) {
				restResponse.setData(existingUserOptional.get());
			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("User does not exist.");
			}

		} catch (Exception e) {
			logger.error("Error occured while getting user.", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while getting user.");
		}

		logger.info("list() : END");
		return restResponse;
	}

	@PostMapping
	public RestResponseDTO create(User user) {
		logger.info("create() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			userService.save(user);
			restResponse.setData(user);
		} catch (Exception e) {
			logger.error("Error occured while creating user", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while creating user");
		}
		logger.info("create() : END");
		return restResponse;
	}

	@PutMapping("/{id}")
	public RestResponseDTO update(User user, @PathVariable long id) {
		logger.info("update() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {

			Optional<User> existingUserOptional = userRepository.findById(id);

			if (existingUserOptional.isPresent()) {
				User existingUser = existingUserOptional.get();
				existingUser.setFirstName(user.getFirstName());
				existingUser.setLastName(user.getLastName());
				existingUser.setEmail(user.getEmail());
				existingUser.getRoles().clear();
				existingUser.getRoles().addAll(user.getRoles());
				existingUser.setEnabled(user.isEnabled());

				userService.save(existingUser);

				restResponse.setData(existingUser);

			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("User dose not exist.");
			}
		} catch (Exception e) {
			logger.error("Error occured while updating user", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while updating user");
		}
		logger.info("update() : END");
		return restResponse;
	}
	
	
	@PutMapping("/{id}/changePassword")
	public RestResponseDTO changePassword(@PathVariable long id, @RequestParam(required = true) String newPassword) {
		logger.info("changePassword() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {

			Optional<User> existingUserOptional = userRepository.findById(id);

			if (existingUserOptional.isPresent()) {
				User existingUser = existingUserOptional.get();
				existingUser.setPassword(newPassword);
				
				userService.save(existingUser);
			} else {
				restResponse.setStatus(false);
				restResponse.setMessage("User dose not exist.");
			}
		} catch (Exception e) {
			logger.error("Error occured while changing password.", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while changing password.");
		}
		logger.info("changePassword() : END");
		return restResponse;
	}

}
