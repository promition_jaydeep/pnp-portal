/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author jjvirani
 *
 */
public abstract class BaseRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseRestController.class);
	
}
