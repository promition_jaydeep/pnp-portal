/**
 * 
 */
package org.gov.patannagarpalika.controller.rest;

import org.gov.patannagarpalika.dto.RestResponseDTO;
import org.gov.patannagarpalika.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jjvirani
 *
 */
@RestController
@RequestMapping("/api/roles")
public class RoleRestController extends BaseRestController {
	
	private static Logger logger = LoggerFactory.getLogger(RoleRestController.class);
	
	@Autowired
	private RoleRepository roleRepository;
	
	@RequestMapping(path= {""})
	public RestResponseDTO list() {
		logger.info("list() : START");
		RestResponseDTO restResponse = new RestResponseDTO();
		try {
			restResponse.setData(roleRepository.findAll());
		} catch(Exception e) {
			logger.error("Error occured while getting roles", e);
			restResponse.setStatus(false);
			restResponse.setMessage("Error occured while getting roles.");
		}
		logger.info("list() : END");
		
		return restResponse;
	}
	
	
	
}
