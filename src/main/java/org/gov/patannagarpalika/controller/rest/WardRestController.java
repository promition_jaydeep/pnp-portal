package org.gov.patannagarpalika.controller.rest;

import org.gov.patannagarpalika.constant.AppConstants;
import org.gov.patannagarpalika.dto.RestResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/wards")
public class WardRestController extends BaseRestController {

    private static Logger logger = LoggerFactory.getLogger(ComplaintTypeRestController.class);

    @GetMapping
    public RestResponseDTO readAll() {
        logger.info("readAll() : START");
        RestResponseDTO restResponse = new RestResponseDTO();
        try {
            restResponse.setData(AppConstants.WARDS);
        } catch (Exception e) {
            logger.error("Error occurred while getting wards", e);
            restResponse.setStatus(false);
            restResponse.setMessage("Error occurred while getting wards.");
        }
        logger.info("readAll() : END");

        return restResponse;
    }
}
