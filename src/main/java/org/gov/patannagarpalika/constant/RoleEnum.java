/**
 * 
 */
package org.gov.patannagarpalika.constant;

/**
 * @author jjvirani
 *
 */
public enum RoleEnum {
	
	ADMIN ("Admin", "ROLE_ADMIN"),
	USER ("User", "ROLE_USER");
	
	private String displayName;
	private String role;
	
	private RoleEnum(String displayName, String role) {
		this.displayName = displayName;
		this.role = role;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public String getRole() {
		return role;
	}
}
