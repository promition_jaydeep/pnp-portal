/**
 * 
 */
package org.gov.patannagarpalika.constant;

/**
 * @author jjvirani
 *
 */
public enum ComplaintStatusEnum {
	
	OPEN ("Open"),
	IN_PROGRESS ("In Progress"),
	CLOSED ("Closed");
	
	private String displayName;
	
	private ComplaintStatusEnum(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
	
}
