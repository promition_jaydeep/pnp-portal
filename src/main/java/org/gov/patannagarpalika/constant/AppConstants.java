/**
 * 
 */
package org.gov.patannagarpalika.constant;

import java.time.format.DateTimeFormatter;

/**
 * @author jjvirani
 *
 */
public interface AppConstants {
	
	String COMPLAINT_TYPE_OTHER = "અન્ય (Other)";
	
	int COMPLAINT_RESOLUTION_DAYS = 5;
	
	String CURRENT_USER = "currentUser";
	
	String USERNAME_PATTERN = "^[a-zA-Z0-9_-]$";
	
	String EMAIL_PATTERN = "^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";

	String LOCAL_DATE_FORMAT = "dd/MM/yyyy";
	DateTimeFormatter LOCAL_DATE_FORMATTER = DateTimeFormatter.ofPattern(LOCAL_DATE_FORMAT);
	
	String[] WARDS = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

}
