/**
 * 
 */
package org.gov.patannagarpalika.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author jjvirani
 *
 */
@Entity
@Table(name="complaint_comments")
public class ComplaintComment extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "complaint_id", nullable= false)
	private Complaint complaint;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(length=1000)
	private String text;

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
