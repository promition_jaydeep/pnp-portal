package org.gov.patannagarpalika.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.gov.patannagarpalika.constant.ComplaintStatusEnum;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "complaints")
public class Complaint extends BaseModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "complaint_no", nullable= false)
	private String complaintNo;

	@JsonView(DataTablesOutput.View.class)
	@Column(name = "ward_no", nullable= false)
	private String wardNo;
	
	@JsonView(DataTablesOutput.View.class)
	@ManyToOne
	@JoinColumn(name = "complaint_type_id", nullable= false)
	private ComplaintType complaintType;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "text")
	@Lob
	private String text;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "contact_no", nullable = false)
	private String contactNo;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "email")
	private String email;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "name", nullable = false)
	private String name;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(name = "address", nullable = false)
	private String address;
	
	@JsonView(DataTablesOutput.View.class)
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private ComplaintStatusEnum status;

	@JsonView(DataTablesOutput.View.class)
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "attachment_id")
	private ComplaintAttachment attachment;
	
	@JsonManagedReference
	@JsonView(DataTablesOutput.View.class)
	@OneToMany(
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true,
	        mappedBy = "complaint",
	        fetch = FetchType.EAGER
	    )
	@OrderBy("createdDate DESC")
	private List<ComplaintComment> comments = new LinkedList<>();

	@JsonView(DataTablesOutput.View.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date resolutionDate;

	public String getComplaintNo() {
		return complaintNo;
	}

	public void setComplaintNo(String complaintNo) {
		this.complaintNo = complaintNo;
	}

	public String getWardNo() {
		return wardNo;
	}

	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}

	public ComplaintType getComplaintType() {
		return complaintType;
	}

	public void setComplaintType(ComplaintType complaintType) {
		this.complaintType = complaintType;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ComplaintStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ComplaintStatusEnum status) {
		this.status = status;
	}

	public ComplaintAttachment getAttachment() {
		return attachment;
	}

	public void setAttachment(ComplaintAttachment attachment) {
		this.attachment = attachment;
	}

	public List<ComplaintComment> getComments() {
		return comments;
	}

	public void setComments(List<ComplaintComment> comments) {
		this.comments = comments;
	}

	public Date getResolutionDate() {
		return resolutionDate;
	}

	public void setResolutionDate(Date resolutionDate) {
		this.resolutionDate = resolutionDate;
	}
	
	public void addComment(ComplaintComment comment) {
		comments.add(comment);
		comment.setComplaint(this);
	}
	
	public void addAllComments(List<ComplaintComment> comments) {
		if(comments != null) {
			comments.forEach(comment -> addComment(comment));
		}
	}

	public void removeComment(ComplaintComment comment) {
		comments.remove(comment);
		comment.setComplaint(null);
	}
	
	public void removeAllComments(List<ComplaintComment> comments) {
		if(comments != null) {
			comments.forEach(comment -> removeComment(comment));
		}
	}
}
