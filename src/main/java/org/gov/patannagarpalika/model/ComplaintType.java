package org.gov.patannagarpalika.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "complaint_types")
public class ComplaintType extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonView(DataTablesOutput.View.class)
	@Column
	private String name;
	
	@JsonView(DataTablesOutput.View.class)
	@Column
	private int resolutionDays;
	
	@JsonView(DataTablesOutput.View.class)
	@ManyToOne
	@JoinColumn(name = "assign_to")
	private User assignTo;

	public ComplaintType() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getResolutionDays() {
		return resolutionDays;
	}

	public void setResolutionDays(int resolutionDays) {
		this.resolutionDays = resolutionDays;
	}

	public User getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(User assignTo) {
		this.assignTo = assignTo;
	}

}
