/**
 * 
 */
package org.gov.patannagarpalika.model;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author jjvirani
 *
 */
@Entity
@Table(name = "users")
public class User extends BaseModel implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonView(DataTablesOutput.View.class)
	@NotBlank
	@Column(unique=true, nullable=false)
	private String username;
	
	@JsonIgnore
	@NotBlank
	@Column
	private String password;
	
	@JsonIgnore
	transient private String confirmPassword;
	
	@JsonView(DataTablesOutput.View.class)
	@Column
	private String email;
	
	@JsonView(DataTablesOutput.View.class)
	@NotBlank
	@Column
	private String firstName;
	
	@JsonView(DataTablesOutput.View.class)
	@NotBlank
	@Column
	private String lastName;
	
	@JsonView(DataTablesOutput.View.class)
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	
	transient List<GrantedAuthority> athorities;
	
	@Column
	private boolean accountNonExpired = true;
	
	@Column
	private boolean accountNonLocked = true;
	
	@Column
	private boolean credentialsNonExpired = true;
	
	@JsonView(DataTablesOutput.View.class)
	@Column
	private boolean enabled = false;
	
	public User() {}
	

	public void addRole (Role role) {
		roles.add(role);
	}
	
	public void removeRole (Role role) {
		roles.remove(role);
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// Convert User roles to Security User authorities
		
		if (CollectionUtils.isNotEmpty(roles) && CollectionUtils.isEmpty(athorities)) {
			if(athorities == null) {
				athorities = new ArrayList<>(1);
			}
			roles.stream().forEach(role -> athorities.add(new SimpleGrantedAuthority(role.getName())));
		}
		
		return athorities;
	}
	
	public boolean hasRole(String roleName) {
		boolean hasRole = false;
		
		if (StringUtils.isNotBlank(roleName) && CollectionUtils.isNotEmpty(roles)) {
			hasRole = roles.stream().filter(role -> roleName.equalsIgnoreCase(role.getName())).findFirst().isPresent();

		}
		
		return hasRole;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}
	
	public void setAccountNonExpired(boolean accountNonExpired) {
		 this.accountNonExpired = accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
	
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
