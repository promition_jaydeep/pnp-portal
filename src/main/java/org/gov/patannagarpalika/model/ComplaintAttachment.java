/**
 * 
 */
package org.gov.patannagarpalika.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;

/**
 * @author jjvirani
 *
 */
@Entity
@Table(name="complaint_attachments")
public class ComplaintAttachment extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "complaint_id", nullable= false)
	private Complaint complaint;
	
	@JsonView(DataTablesOutput.View.class)
	@Column(length=255)
	private String fileName;

	@JsonView(DataTablesOutput.View.class)
	@Column(length=255)
	private String fileType;

	@Lob
	@JsonIgnore
	private byte[] data;

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}
