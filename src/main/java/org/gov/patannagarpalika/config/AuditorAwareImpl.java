/**
 * 
 */
package org.gov.patannagarpalika.config;

import java.util.Optional;

import org.gov.patannagarpalika.model.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;



/**
 * @author jjvirani
 *
 */
public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		Optional<String> currentUser = Optional.of("");
		
		
		if(auth != null) {
			Object authUser = auth.getPrincipal();
			
			if(authUser != null) {
				
				if(authUser instanceof User) {
					currentUser = Optional.of(((User)authUser).getUsername());
				} else {
					currentUser = Optional.of(authUser.toString());
				}
			}
		} 
		
		return currentUser;
	}
	

}
