/**
 * 
 */
package org.gov.patannagarpalika.config;

import org.gov.patannagarpalika.service.MetaDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * @author jjvirani
 *
 */
@Component
@Order(1)
public class DataInitializer {
	
	@Lazy
	@Autowired
	private MetaDataService metadataService;


	@EventListener(ApplicationReadyEvent.class)
	public void createMetadata() {
		metadataService.createMetadata();
	}
	

}
