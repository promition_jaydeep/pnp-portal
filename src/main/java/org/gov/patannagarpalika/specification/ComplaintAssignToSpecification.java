/**
 * 
 */
package org.gov.patannagarpalika.specification;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.gov.patannagarpalika.model.Complaint;
import org.gov.patannagarpalika.model.ComplaintType;
import org.springframework.data.jpa.domain.Specification;

import com.querydsl.core.group.Group;

/**
 * @author jjvirani
 *
 */
public class ComplaintAssignToSpecification implements Specification<Complaint>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ComplaintType> filter;

    public ComplaintAssignToSpecification(List<ComplaintType> filter) {
        super();
        this.filter = filter;
    }

	@Override
	public Predicate toPredicate(Root<Complaint> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
		Predicate p = cb.disjunction();

        if (filter != null) {
        	final Path<Group> group = root.<Group> get("complaintType");
            p.getExpressions().add(group.in(filter));
        }

        return p;

	}

}
